from src.utils.NotifyManager import SenderBase, register_sender


__all__ = ['SenderHandler']


class SenderHandler(SenderBase):
    handler_name = 'telegram'
    methods = {
        'send': ('http://telegram.org/API/send', 'POST'),
        'status': ('http://telegram.org/API/status', 'GET')
    }

    def define_format(self):
        """
        Define the response format as JSON
        :return:
        """
        self.set_format('JSON')


register_sender(2, SenderHandler, 3600)
