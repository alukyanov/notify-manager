from django.conf import settings
from django.core.mail import send_mail

from src.utils.NotifyManager import SenderBase, register_sender


__all__ = ['SenderHandler']


class SenderHandler(SenderBase):
    handler_name = 'email'

    def define_format(self):
        """
        Define the response format as JSON
        :return:
        """
        self.set_format('JSON')

    def _send_by_email(self, *args, **kwargs):
        email_to = kwargs['recipient']
        from_email = kwargs.get('from_email',
                                getattr(settings, 'DEFAULT_FROM_EMAIL', None))
        if not all((email_to, from_email)):
            return {'error': 'error'}
        subject = kwargs.get('subject', '')
        messages = kwargs.get('message')
        html_messages = kwargs.get('html_message')
        if not messages:
            return {'error': 'message not defined'}
        send_mail(subject, messages, from_email, [email_to],
                  html_message=html_messages)
        return {'status': True}

    def _status_by_email(self, *args, **kwargs):
        return {'status': True}

    methods = {
        'send': _send_by_email,
        'status': _status_by_email
    }


register_sender(0, SenderHandler)
