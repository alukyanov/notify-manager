from src.utils.NotifyManager import SenderBase, register_sender


__all__ = ['SenderHandler']


class SenderHandler(SenderBase):
    handler_name = 'smsc'
    methods = {
        'send': ('http://smsc.ru/sys/send.php', 'GET'),
        'status': ('http://smsc.ru/sys/status.php', 'GET'),
        'balance': ('http://smsc.ru/sys/balance.php', 'GET')
    }

    def define_format(self):
        fmt = int(self.params.get('fmt')) if 'fmt' in self.params else None
        if fmt == 2:
            self.set_format('XML')
        elif fmt == 3:
            self.set_format('JSON')

    def handle_error(self, data):
        code, message = None, None
        if self.is_xml:
            pass
        elif self.is_json and 'error' in data:
            code, message = data['error'], data['error_code']
        elif self.is_plaintext and data.find('ERROR') >= 0:
            code, message = data.split(' = ')[1].split(' ', 1)
        return code, message


register_sender(3, SenderHandler, 3600)
