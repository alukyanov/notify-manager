from src.utils.NotifyManager import SenderBase, register_sender


__all__ = ['SenderHandler']


class SenderHandler(SenderBase):
    handler_name = 'whatsapp'

    def define_format(self):
        """
        Define the response format as JSON
        :return:
        """
        self.set_format('JSON')

    def _send_by_yowsup(self, *args, **kwargs):
        return {'status': True}

    def _status_by_yowsup(self, *args, **kwargs):
        return {'status': True}

    methods = {
        'send': _send_by_yowsup,
        'status': _status_by_yowsup
    }


register_sender(1, SenderHandler, 3600)
