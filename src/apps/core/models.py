from uuid import uuid4
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

from src.utils.NotifyManager import get_senders, send_message


__all__ = ['UserProfile']


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone = models.CharField(max_length=20, blank=True, null=True)
    sender = models.CharField(max_length=20, blank=True, null=True)

    def send_message(self, message, subject=None):
        SenderQueue.objects.send(self, message, subject=subject)


class SentMessage(models.Model):
    created = models.DateTimeField(
        default=timezone.now, auto_now_add=True, blank=True)
    profile = models.ForeignKey(UserProfile, related_name="senders_list")
    subject = models.CharField(max_length=255, blank=True, null=True)
    message = models.TextField()


class SenderQueueManager(models.Manager):
    def get_sender(self, profile):
        """
        Returns confirmed sender name.
        If sender still doesn't defined, the message will be stored into queue
        to being confirmed by user
        :param profile:
        :return:
        """
        if profile.sender:
            return profile.sender
        confirmed = self.filter(profile=profile).exclude(
            confirmed=None).order_by('ordering')
        if confirmed.exists():
            return confirmed[0].sender
        return None

    def send(self, profile, message, subject=None):
        sender = self.get_sender(profile)
        message = SentMessage.objects.create(
            profile=profile, message=message, subject=subject)
        if sender is None:
            for ordering, sender, ttl in get_senders():
                if ordering == 0:  # pass required senders
                    continue
                self.create(profile=profile, message=message, sender=sender,
                            ordering=ordering, ttl=ttl)
        else:
            send_message(sender, phone=profile.phone, email=profile.user.email,
                         message=message.message, subject=subject)

    def confirm(self, message_uuid):
        """
        Confirm sender as default
        :param message_uuid:
        :return:
        """
        try:
            sender = self.get(uuid=message_uuid)
            sender.confirmed = timezone.now()
            sender.save()
            profile = sender.profile
            profile.sender = sender.sender
            profile.save()
        except SenderQueue.DoesNotExist:
            pass

    def next_sender(self, profile):
        """
        Select next sender and send the assigned message
        :param profile:
        :return:
        """
        now = timezone.now()
        senders = self.filter(profile=profile).order_by('ordering')
        if senders.exists():
            for sender in senders:
                if sender.sent and (now - sender.sent) < sender.ttl:
                    break  # break if the time has passed less than defined for response
                if sender.sent is None:
                    sender.send()
                    break  # break if it's time to next sender


class SenderQueue(models.Model):
    uuid = models.CharField(max_length=36, default=str(uuid4()), blank=True)
    created = models.DateTimeField(
        default=timezone.now, auto_now_add=True, blank=True)
    sent = models.DateTimeField(blank=True, null=True)
    profile = models.ForeignKey(UserProfile, related_name="senders_list")
    message = models.ForeignKey(SentMessage)
    sender = models.CharField(max_length=10, blank=True, null=True)
    ordering = models.PositiveSmallIntegerField(default=0, blank=True)
    ttl = models.PositiveIntegerField(default=3600, blank=True)
    confirmed = models.DateTimeField(
        default=timezone.now, blank=True, null=True)

    objects = SenderQueueManager()

    class Meta:
        ordering = ('profile', 'ordering',)

    def __unicode__(self):
        return self.sender

    def send(self):
        send_message(self.sender, phone=self.profile.phone,
                     email=self.profile.user.email,
                     message=self.message.message, subject=self.message.subject)
        self.sent = timezone.now()
        self.save()

