from django.views.generic.base import View
from django.http import JsonResponse

from .models import SenderQueue


class ConfirmView(View):
    def get(self, request, *args, **kwargs):
        uuid = self.kwargs['uuid']
        SenderQueue.objects.confirm(uuid)
        return JsonResponse({'status': True})
confirm_view = ConfirmView.as_view()
