from django.core.management.base import BaseCommand

from src.apps.core.models import SenderQueue, UserProfile


class Command(BaseCommand):
    def handle(self, *args, **options):
        profiles = UserProfile.objects.filter(
            sender=None, user__is_active=True).exclude(phone=None)
        next_sender = SenderQueue.objects.next_sender
        for profile in profiles:
            next_sender(profile)
