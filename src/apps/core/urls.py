from django.conf.urls import patterns, url


core_urlpatterns = patterns(
    'src.apps.core.views',
    url(r'^SenderConfirmation/(?P<uuid>\w+)$', 'confirm_view'),
)
