import json
import urllib
import urllib2
from copy import copy
from .exceptions import *


__all__ = ['send_message', 'SenderBase', 'register_sender', 'get_senders']


SENDERS = []


def send_message(sender, **kwargs):
    """Send message by sender name
    :param handler_name: SMSGW name
    :return:
    """
    senders = get_senders()
    senders = [s[1] for s in senders if s[0] == 0]
    senders.extend([s[1] for s in senders if s[1].handler_name == sender])
    for sender in senders:
        sender.send(**kwargs)


def get_senders():
    """
    Returns a copy of senders list to prevent any changes of it
    :return: list
    """
    global SENDERS
    return copy(SENDERS)


def register_sender(sender_class, ordering, ttl=None):
    """
    Register a message sender in global variable
    :param sender_class:
    :param ordering:
    :param ttl: time to live
    :return:
    """
    global SENDERS
    SENDERS.append((ordering, sender_class, ttl))


class SenderBase(object):
    """
    Base class
    """
    RESPONSE_FORMAT = (
        (0, 'PLAIN-TEXT'),
        (1, 'XML'),
        (2, 'JSON'),
    )
    handler_name = None
    params = {}
    methods = {}
    _headers = {}
    _response_format = 0    # 0 - PLAIN-TEXT
                            # 1 - XML
                            # 2 - JSON
    encoding = 'utf-8'
    logger = None

    def _call(self, method, params):
        """Request GW API method
        :param url:
        :param method:
        :return:
        """
        if method not in self.methods:
            raise MethodNotFound('Method \'{0}\' not found.'.format(method))

        m = self.methods[method]
        self.params.update(params)

        if not isinstance(m, (tuple, list)):  # over custom function
            result = m(**self.params)
        else:  # over http
            url, http_method = m
            if http_method in ('POST', 'PUT', 'DELETE'):
                req = urllib2.Request(url, params, self._headers)
            elif http_method == 'GET':
                url += '?{0}'.format(urllib.urlencode(self.params))
                req = urllib2.Request(url, None, self._headers)
            else:
                raise HttpMethodNotAllowed(
                    'Http method \'{0}\' not allowed.'.format(http_method))
            result = urllib2.urlopen(req).read()
            if self.logger:
                self.logger(self.handler_name, method,
                            '{0}\n{1}'.format(url, str(self.params)), result)
        return self._response_parser(result)

    def _is_valid(self, data):
        """Validate response data
        :param data:
        :return:
        """
        error_code, error_message = self.handle_error(data)
        if error_code is not None:
            raise GatewayError(error_code, error_message)

    def _response_parser(self, result):
        """Response parser
        :param data:
        :return:
        """
        self.define_format()
        if self.is_plaintext:
            response = result
        elif self.is_xml:
            response = self.xml_parser(result)
        elif self.is_json:
            try:
                response = json.loads(result)
            except Exception, e:
                raise GatewayError(e.message)
        else:
            raise WrongResponseFormat(
                'Wrong response format: {0}'.format(self._response_format))
        self._is_valid(response)
        return response

    def define_format(self):
        """Return response format
        :param format:
        :return:
        """
        self.set_format('PLAIN-TEXT')

    def set_format(self, fmt):
        """Set response format
        :param format:
        :return:
        """
        if isinstance(fmt, basestring):
            rf = {v: k for k, v in self.RESPONSE_FORMAT}
            fmt = fmt.upper()
            if fmt not in rf:
                raise WrongResponseFormat(
                    'Wrong response format: {0}'.format(fmt))
            self._response_format = rf[fmt]
        elif isinstance(fmt, int):
            if not filter(lambda x: x[0] == fmt, self.RESPONSE_FORMAT):
                raise WrongResponseFormat(
                    'Wrong response format: {0}'.format(fmt))
            self._response_format = fmt
        else:
            raise ResponseFormatNotImplemented(
                'Response format is not implemented: {0}'.format(fmt))

    @property
    def is_json(self):
        return self._response_format == 2

    @property
    def is_xml(self):
        return self._response_format == 1

    @property
    def is_plaintext(self):
        return self._response_format == 0

    def handle_error(self, data):
        """Handle an error from Gateway
        :param data:
        :return:
        """
        raise NotImplementedError()

    def xml_parser(self, data):
        """GW response XML-parser
        :param data:
        :return:
        """
        raise NotImplementedError()

    def set_headers(self, headers):
        self._headers = headers

    def request(self, method, **kwargs):
        """Request not implemented method
        :param method:
        :param kwargs:
        :return:
        """
        return self._call(method, kwargs)

    def send(self, **kwargs):
        """Sends the SMS-message through the SMS-gateway API
        :param phones_list: a list of phones
        :param message: a message
        :return:
        """
        return self._call('send', kwargs)

    def status(self, **kwargs):
        """Checks the status of the sent message
        :param transaction_id:
        :return:
        """
        return self._call('status', kwargs)
