

class BaseException(Exception):
    def __init__(self, message=None, code=None):
        if code:
            self.code = code
        if message:
            self.message = message


class HttpMethodNotAllowed(BaseException):
    code = '11000'
    message = 'Http method not allowed'


class MethodNotFound(BaseException):
    code = '12000'
    message = 'Http method not found'


class WrongResponseFormat(BaseException):
    code = '13000'
    message = 'Wrong response format'


class ResponseFormatNotImplemented(BaseException):
    code = '13001'
    message = 'Response format is not implemented'


class GatewayError(BaseException):
    code = '14000'
    message = 'An exception has occurred'


