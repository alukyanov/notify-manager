Существует базовый класс **src.utils.NotifyManager.SenderBase** на базе которого создаются дочерние классы для каждого сервиса.

Используемые методы:

* **define_format()** - определяет формат данных ответа сервиса, может быть: PLAIN-TEXT (по умолчанию), XML, JSON. Данные метод вызывается в момент парсинга данных ответа. Если не перегузить этот метод, по умолчанию будет установлен PLAIN-TEXT.
* **set_format(fmt)** - устанавливает формат данных ответа сервиса.
* **handle_error(data)** - метод обработки ошибки сервиса, на тот случай, если сервис вернул ошибку.
* **xml_parser(data)** - метод парсинга данных ответа сервиса в формате XML.
* **set_headers(<type: dict>)** - установка дополнительных заголовков для запросов через HTTP к сервису.
* **send(kwargs)** - метод отправки сообщения с параметрами (именованными аргументами).
* **status(kwargs)** - метод проверки статуса, отправленного, сообщения с параметрами (именованными аргументами)

В дочернем классе необходимо указать методы запросов к сервису, например:

```
#!python

    methods = {
        'send': ('http://smsc.ru/sys/send.php', 'GET'),
        'status': ('http://smsc.ru/sys/status.php', 'GET'),
        'balance': ('http://smsc.ru/sys/balance.php', 'GET')
    }

```

допустипо указывать в качестве значений ключей, методы или функции. Это необходимо в тех случаях, когда сервис не поддерживает API по протоколу HTTP, например, отправка email.

```
#!python

    def _send_by_email(self, *args, **kwargs):
        email_to = kwargs['recipient']
        from_email = kwargs.get('from_email',
                                getattr(settings, 'DEFAULT_FROM_EMAIL', None))
        if not all((email_to, from_email)):
            return {'error': 'error'}
        subject = kwargs.get('subject', '')
        messages = kwargs.get('message')
        html_messages = kwargs.get('html_message')
        if not messages:
            return {'error': 'message not defined'}
        send_mail(subject, messages, from_email, [email_to],
                  html_message=html_messages)
        return {'status': True}

    def _status_by_email(self, *args, **kwargs):
        return {'status': True}

    methods = {
        'send': _send_by_email,
        'status': _status_by_email
    }

```

После создания дочернего класса сервиса, необходимо его зарегистрировать в общем списке классов:

```
#!python

from src.utils.NotifyManager import register_sender

register_sender(3, SenderHandler, 3600)
```

где 3 - порядок выполнения (начинаетсяя с 1, когда 0 определяет те сервисы, через которые будут отправлены сообщения в любом случае), SenderHandler - класс сервиса, 3600 - время жизни сообщения в очереди ожидания подтверждения сервиса в качестве основного.

В приложении core созданы: расширенная модель пользователя UserProfile, содержащая дополнительные поля: phone - номер телефона, sender - имя сервиса отправителя; модель SenderQueue, которая хранит очередь для подтверждения сервиса по умолчанию.

Логика очереди: при отправке первичного сообщения, когда UserProfile.sender = None, в SenderQueuer добавляются сервисы для отправки с указанными сортировкой и временем жизни; отправляется сообщение через первый сервис, система (cron) ждет время ttl для подтверждения сервиса в качестве основного; система каждую минуту (cron) выполняет проверку по всем пользователям, не подтвердили ли они тот или иной сервис, если проходит время жизни сервиса, отправляется сообщение через другой и т.д. пока не закончится цикл.